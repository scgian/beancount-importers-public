"""
Importer for Cembra MoneyBank Monthly Credit-Card Statements
In the current version statements are only supported if all transactions are listed on page
2 of the pdf file.
"""
__copyright__ = "Copyright (C) 2019  Gian Nutal"
__license__ = "GNU GPLv2"

import datetime
import re
import pprint
import logging
import os
from os import path
import glob
from PyPDF2 import PdfFileReader
import tabula



from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core.number import Decimal
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import flags
from beancount.ingest import importer
from beancount.ingest import regression
from beancount.ingest.extract import extract_from_file
from beancount.ingest import identify

class CembraImporter(importer.ImporterProtocol):
    """An importer for Cembra MoneyBank monthly credit-card statements"""
    def __init__(self, account, liability = "Liabilities:CH:Cembra"):
        self.account = account # Kontonummer
        self.liability = liability

    def name(self):
        return ("CembraMoneyBank Importer")

    def identify(self,file):
        if re.search(self.account+r"\d\d\d\d\d\d\d\d\.pdf", path.basename(file.name)):
            with open(file.name, 'rb') as f:
                try:
                    assert(PdfFileReader(f.name).getNumPages() == 3)
                    # df = tabula.read_pdf(f.name,pages = '2',multiple_tables=True)[-1]
                except:
                    print('PDF File contains more than 3 pages. File is skipped')
                    return False
                return True
        else:
            return False

    def file_name(self,file):
        return 'CMB.{}'.format(path.basename(file.name))

    def file_account(self, _):
        return self.account

    def file_date(self,file):
        # Extract the statement date from the filename.
        date = path.basename(file.name)[-12:-4]
        return datetime.datetime.strptime(path.basename(date),'%d%m%Y').date()

    def extract(self, file):
        # Open the pdf statements and create directives
        entries = []
        df = tabula.read_pdf(file.name,pages = '2',multiple_tables=True)[-1]
        df.columns = df.iloc[0]
        df = df.drop(0)
        for index, transaction in df.iterrows():
            message = transaction['Beschreibung']
            postings = [data.Posting(
                self.liability,
                amount.Amount(Decimal('-'+transaction['Belastung CHF'].replace("'",'')),'CHF'),
                None,None,None,None
            )]
            entries.append(data.Transaction(
                data.new_metadata(file.name,index),
                datetime.datetime.strptime(transaction['Einkaufs-Datum'],'%d.%m.%Y').date(),
                self.FLAG,
                message,"",
                data.EMPTY_SET,
                data.EMPTY_SET,
                postings,
                )
            )
        return entries

if __name__ == "__main__":
    folder = r"C:\Users\username\Downloads"
    file = glob.glob(os.path.join(folder,'*.pdf'))
    CONFIG = [CembraImporter('123456789')]
    identify.identify(CONFIG,r"C:\Users\username\Downloads")
    extract_from_file(file[2],CONFIG[0])