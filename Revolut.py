"""
This importer is based on a post in the beancount mailinglist
The importer is made to support the german version of the statement. To change the language support to english comment line 11
Also, you should not have a statement that has transaction dates from two or more different years.
"""

from dateutil.parser import parse
import datetime
import re
import locale
locale.setlocale(locale.LC_ALL, "deu_deu")
from io import StringIO
from os import path

from beancount.ingest import importer
from beancount.core import data
from beancount.core import amount
from beancount.core.number import D
from beancount.ingest.importers.mixins import identifier

import csv

class Importer(importer.ImporterProtocol):
    """An importer for Revolut CSV files."""

    def __init__(self, account=None, currency=None,file_encoding='utf-8'):
        self.account = account
        self.currency = currency
        self.file_encoding = file_encoding

    def name(self):
        return ('Revolut Importer')

    def identify(self, file):
        # Match if the filename is as downloaded and the header has the unique
        # fields combination we're looking for.
        if re.search(r"Revolut",path.basename(file.name)):
            self.currency = file.name.split('-')[1]
            if self.currency == 'CHF':
                self.account = "Assets:GBR:Revolut:CHF"
            elif self.currency == 'USD':
                self.account = "Assets:GBR:Revolut:USD"
            else:
                raise('Currency is not supported yet in the importer. Please add 2 lines of code in the function identify')
            return True
        else:
            return False

    def file_date(self,file):
        # Extract the year from the filename
        return path.basename(file.name).split('.')[-2]

    def file_account(self, file):
        return self.account

    def extract(self, file, existing_entries):
        entries = []

        with StringIO(file.contents()) as csvfile:
            reader = csv.DictReader(csvfile, ['Date', 'Reference','PaidOut', 'PaidIn', 'ExchangeOut', 'ExchangeIn', 'Balance', 'Category'], delimiter=';', skipinitialspace=True)
            next(reader)
            for row in reader:
                # if re.search(r"Sparen",row['Reference'].strip()):
                #     continue
                metakv = {
                    'category': row['Category'].strip(),
                }
                exchangeIn = row['ExchangeIn'].strip()
                exchangeOut = row['ExchangeOut'].strip()
                if exchangeIn and exchangeOut:
                    metakv['originalIn'] = exchangeIn
                    metakv['originalOut'] = exchangeOut
                elif exchangeIn:
                    metakv['original'] = exchangeIn
                elif exchangeOut:
                    metakv['original'] = exchangeOut
                if len(row['Date'].strip().split(' ')) == 3:
                    date = datetime.datetime.strptime(row['Date'].strip(),'%Y %B %d').date()
                else:
                    date = (datetime.datetime.strptime(row['Date'].strip()+self.file_date(file),'%B %d %Y')).date()

                meta = data.new_metadata(file.name, 0, metakv)
                postings = [data.Posting(self.account, amount.Amount(D(row['PaidIn'].replace("’",'').strip()) - D(row['PaidOut'].replace("’",'').strip()), self.currency), None, None, None, None)]
                entries.append(data.Transaction(
                    meta,
                    date,
                    '*',
                    row['Reference'].strip(),"",
                    data.EMPTY_SET,
                    data.EMPTY_SET,
                    postings,
                    )
                )
        return entries