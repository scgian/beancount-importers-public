"""
Importer for Swisscard monthly Credit-Card Statements
"""
__copyright__ = "Copyright (C) 2019  Gian Nutal"
__license__ = "GNU GPLv2"

import datetime
import re
import pprint
import logging
import os
from os import path
import glob
from PyPDF2 import PdfFileReader
import tabula



from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core.number import Decimal
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import flags
from beancount.ingest import importer
from beancount.ingest import regression
from beancount.ingest.extract import extract_from_file
from beancount.ingest import identify

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import StringIO
import pandas as pd


class SwisscardImporter(importer.ImporterProtocol):
    """An importer for Swisscard monthly credit-card statements"""
    def __init__(self, account, liability = "Liabilities:CH:Swisscard"):
        self.account = account # Kontonummer
        self.liability = liability

    def get_data(self,path):
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        codec = 'utf-8'
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        with open(path, 'rb') as fp:
            interpreter = PDFPageInterpreter(rsrcmgr, device)
            password = ""
            maxpages = 0
            caching = True
            pagenos=set()

            for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
                interpreter.process_page(page)

            text = retstr.getvalue()
        device.close()
        retstr.close()
        tokens = text.split('\n')
        account = tokens[31].replace(' ','')
        start = tokens.index('Datum') #start of table
        delta_start = tokens.index('Neue Transaktionen')-start+4 #delta between start and start_transactions
        start_transaction = start+delta_start #start of transaction table
        end_transaction = tokens[(start+delta_start):].index('')+start+delta_start #end of transaction on page 1
        col1 = tokens[start_transaction:end_transaction]
        col2 = tokens[end_transaction+1:end_transaction+1+len(col1)]

        start_col3 = tokens.index('Betrag in CHF')
        #No incoming payments
        if delta_start == 8:
            col3 = tokens[start_col3+1:start_col3+1:len(col1)]
        else:
            payment_counter = len([token for token in tokens if (token == 'IHRE ZAHLUNG – BESTEN DANK')])
            start_t_col3 = start_col3+payment_counter+3
            col3 = tokens[start_t_col3:start_t_col3+len(col1)]
        if 'Zwischentotal' in tokens:
            start_of_cols = [index for index,token in enumerate(tokens) if '\x0c' in token][:-3]
            for index in start_of_cols:
                tokens[index] = tokens[index].replace('\x0c','') # remove newpage identnifier from col1
                end_col1 = tokens[index:].index('')+index
                col1.extend(tokens[index:end_col1])
                len_col1 = len(tokens[index:end_col1])
                col2.extend(tokens[end_col1+1:end_col1+1+len_col1])
                col3_start = tokens[index:].index('Übertrag')+4+index
                col3.extend(tokens[col3_start:col3_start+len_col1])
        df = pd.DataFrame(data=list(zip(col1,col2,col3)),columns=['date','description','amount'])
        return account,df

    def name(self):
        return ("Swisscard Importer")

    def identify(self,file):
        if re.search(r"ViewEpsiiaEStatementDetail", path.basename(file.name)):
            try:
                data = self.get_data(file.name)
                assert(data[0] == self.account)
                # df = tabula.read_pdf(f.name,pages = '2',multiple_tables=True)[-1]
            except:
                print('Wrong account. PDF is skipped')
                return False
            return True
        else:
            return False

    def file_name(self,file):
        return 'CMB.{}'.format(path.basename(file.name))

    def file_account(self, _):
        return self.account

    def file_date(self,file):
        # Extract the statement date from the filename.
        date = path.basename(file.name)[-12:-4]
        return datetime.datetime.strptime(path.basename(date),'%d%m%Y').date()

    def extract(self, file):
        # Open the pdf statements and create directives
        entries = []
        df = self.get_data(file.name)[1]
        for index, transaction in df.iterrows():
            message = transaction['description']
            postings = [data.Posting(
                self.liability,
                amount.Amount(Decimal('-'+transaction['amount'].replace("'",'')),'CHF'),
                None,None,None,None
            )]
            entries.append(data.Transaction(
                data.new_metadata(file.name,index),
                datetime.datetime.strptime(transaction['date'],'%d.%m.%Y').date(),
                self.FLAG,
                message,"",
                data.EMPTY_SET,
                data.EMPTY_SET,
                postings,
                )
            )
        return entries

if __name__ == "__main__":
    folder = r'C:\Users\username\Downloads'
    file = glob.glob(os.path.join(folder,'*.pdf'))
    CONFIG = [SwisscardImporter('123456789')]
    identify.identify(CONFIG,r"C:\Users\username\Downloads")
    extract_from_file(file[1],CONFIG[0])