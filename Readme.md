Add the folder myimporters to your virtualenvironment python path in activate.bat

set PYTHONPATH=C:\Users\username\Documents\Beancount\myimporters

command for importing statements in downloads folder

bean-identify example.import C:\Users\username\Downloads

bean-extract -e example.beancount example.import C:\Users\username\Downloads > tmp.beancount

Example.import file.

import sys

from os import path

sys.path.insert(0, path.join(path.dirname(__file__)))

from beancount.ingest import extract

from beancount_importers import Raiffeisenbank

from beancount_importers import Cembra

from beancount_importers import Swisscard

from beancount_importers import Revolut

from smart_importer import apply_hooks, PredictPostings

from smart_importer.detector import DuplicateDetector


CONFIG = [apply_hooks(Raiffeisenbank.RBImporter('Account Holder','Mitglieder Privatkonto'),[PredictPostings(),DuplicateDetector()]),
    apply_hooks(Raiffeisenbank.RBImporter('Account Holder','Mitglieder Sparkonto'),[PredictPostings(),DuplicateDetector()]),
    apply_hooks(Cembra.CembraImporter('123456789'),[PredictPostings(),DuplicateDetector()]),
    apply_hooks(Swisscard.SwisscardImporter('123456789'),[PredictPostings(),DuplicateDetector()]),
    apply_hooks(Revolut.Importer(),[PredictPostings(),DuplicateDetector()]),
    ]
