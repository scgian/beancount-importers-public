"""
Importer for Raiffeisenbank .mt940 account documents.
In Raiffeisen E-Banking use the option to download a single file for all your accounts.
"""
__copyright__ = "Copyright (C) 2019  Gian Nutal"
__license__ = "GNU GPLv2"

import datetime
import re
import pprint
import logging
import os
from os import path
import mt940
import glob


from beancount.core.number import D
from beancount.core.number import ZERO
from beancount.core.number import MISSING
from beancount.core import data
from beancount.core import account
from beancount.core import amount
from beancount.core import position
from beancount.core import flags
from beancount.ingest import importer
from beancount.ingest import regression
from beancount.ingest.extract import extract_from_file
from beancount.ingest import identify


class RBImporter(importer.ImporterProtocol):
    """An importer for Raiffeisenbank mt940 files"""
    def __init__(self, user, account, asset = None, file_encoding='ISO 8859-1'):
        self.user = user
        self.account = account
        self.file_encoding = file_encoding

    def name(self):
            return ('Raiffeisenbank Importer')

    def stat_n_accounts(self,file):
        ''' 
        Returns number of accounts in statement file
        '''
        with open(file.name) as f:
            lines = f.readlines()
            return len([line for line in lines if r':20:STARTUMS' in line]) #return number of accounts in file

    def identify(self, file):
        # Match if the filename is as downloaded and the header has the unique
        # fields combination we're looking for.
        if re.match(r"Konto_\d\d\d\d\d\d\d\d\d\d\d\d\d\d\.mt940", path.basename(file.name)):
            self.split_sta_data(file)
            return False
        if re.match(r"CH\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\d\_\d\d\d\d\d\d\d\d\d\d\d\d\d\d\.mt940",path.basename(file.name)):
            with open(file.name, encoding=self.file_encoding) as fd:
                try:
                    transactions = mt940.parse(fd.name,encoding=self.file_encoding)
                    if transactions.data['non_swift_22'] != self.user or transactions.data['non_swift_23'] != self.account:
                        return False
                except UnicodeDecodeError:
                    return False
                if transactions.data['non_swift_23'] == 'Mitglieder Privatkonto':
                    self.asset = "Assets:CH:Raiffeisenbank:Privatkonto"
                elif transactions.data['non_swift_23'] == 'Mitglieder Sparkonto':
                    self.asset = "Assets:CH:Raiffeisenbank:Sparkonto"
                else:
                    self.asset = "Assets:CH:Raiffeisenbank"
                return transactions
        else:
            return False

    def split_sta_data(self,file):
        '''
        Returns list of text files that contain the data from the different accounts
        '''
        with open(file.name,'r') as f:
            accounts = [None]*self.stat_n_accounts(file)
            i = 0
            for line in f:
                if re.match(r':25:',line.strip()):
                    accounts[i] = line[len(r':25:'):-2]
                    i += 1
        with open(file.name,'r') as f:
                data = f.read().split(r':20:STARTUMS')

        for i,konto in enumerate(accounts,1):
            with open(os.path.join(os.path.dirname(file.name),konto+os.path.basename(file.name)[len('Konto'):]),'w+') as f:
                f.write(r':20:STARTUMS'+data[i])
            accounts[i-1] = os.path.join(os.path.dirname(file.name),konto+os.path.basename(file.name)[len('Konto'):])
        return accounts 

    def file_name(self, file):
        return 'RB.{}'.format(path.basename(file.name))

    def file_account(self, _):
        return self.user,self.account

    def file_date(self, file):
        # Extract the statement date from the filename.
        return datetime.datetime.strptime(path.basename(file.name),
                                          'Konto_%Y%m%d%H%M%S.mt940').date()

    def extract(self, file):
        # Open the mt940 file and create directives.
        entries = []
        transactions = mt940.parse(file.name,encoding=self.file_encoding)
        for index,transaction in enumerate(transactions.transactions):
            message  = [nonswift[2:] for nonswift in transaction.data['non_swift'].split('\n'[-1]) if nonswift[2:]]
            message = ' '.join(message)
            payee = transaction.data['non_swift_17']
            message.encode(encoding='UTF-8')
            postings = [data.Posting(self.asset,amount.Amount(transaction.data['amount'].amount,transaction.data['amount'].currency),
                None,None,None,None)]
            entries.append(data.Transaction(
                data.new_metadata(file.name,index),
                transaction.data['date'],
                self.FLAG,
                payee,message,
                data.EMPTY_SET,
                data.EMPTY_SET,
                postings,
                )
            )
        return entries
    
if __name__ == "__main__":
    # use for bufixing te code, replace username with your username and account holder with the real account holder name
    folder = r'C:\Users\username\Downloads'
    file = glob.glob(os.path.join(folder,'*.mt940'))
    CONFIG = [RBImporter('Account Holder','Mitglieder Sparkonto'),
    RBImporter('Account Holder','Mitglieder Privatkonto')]
    # identify.identify(CONFIG, r"C:\Users\scgian\Downloads")
    extract_from_file(file[0],CONFIG[0])